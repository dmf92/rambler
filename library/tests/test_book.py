import json

import unittest
from django.test import Client


class TestBooks(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def post(self, url, data={}):
        return self.client.post(
            url,
            json.dumps(data),
            content_type='application/json'
        )

    def test_create_book(self):
        resp = self.post(
            '/book',
            {
                'title': 'Some book'
            }
        )

