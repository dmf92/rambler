# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.TextField()),
                ('birth_date', models.DateField()),
                ('death_date', models.DateField()),
                ('birth_country', models.TextField()),
                ('photo', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.TextField()),
                ('genre', models.TextField()),
                ('num_of_pages', models.IntegerField()),
                ('isbn', models.CharField(max_length=13)),
                ('description', models.TextField()),
                ('rating', models.FloatField()),
                ('year', models.IntegerField()),
                ('cover', models.TextField()),
                ('authors', models.ManyToManyField(to='library.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created', models.DateField()),
                ('text', models.TextField()),
                ('book', models.ForeignKey(to='library.Book')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='book',
            name='tags',
            field=models.ManyToManyField(to='library.Tag'),
        ),
    ]
