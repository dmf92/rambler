from django.db.models import (
    Model,
    CharField,
    TextField,
    ForeignKey,
    IntegerField,
    FloatField,
    DateField
)


class Author(Model):
    name = TextField()
    birth_date = DateField()
    death_date = DateField()
    birth_country = TextField()
    # countries
    photo = TextField()
