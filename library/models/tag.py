from django.db.models import (
    Model,
    CharField,
    TextField,
    ForeignKey,
    IntegerField,
    FloatField,
    ManyToManyField
)


class Tag(Model):
    title = TextField()
