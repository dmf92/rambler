from django.db.models import (
    Model,
    CharField,
    TextField,
    ForeignKey,
    IntegerField,
    FloatField,
    DateField
)

from library.models.book import Book


class Review(Model):
    created = DateField()
    text = TextField()

    book = ForeignKey(Book)
