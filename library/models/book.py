from django.db.models import (
    Model,
    CharField,
    TextField,
    ForeignKey,
    IntegerField,
    FloatField,
    ManyToManyField
)

from library.models.author import Author
from library.models.tag import Tag


class Book(Model):
    title = TextField()
    genre = TextField()
    tags = ManyToManyField(Tag)
    num_of_pages = IntegerField()
    isbn = CharField(max_length=13)
    description = TextField()
    authors = ManyToManyField(Author)
    rating = FloatField()
    year = IntegerField()
    cover = TextField()
